import requests
import datetime
import pandas as pd
import sqlite3
import sqlalchemy

DATABASE_LOCATION = "sqlite:///played_tracks.sqlite"
USER_ID = "g0vnls4fz48tvyhox3cfh429v"
TOKEN = "BQCCrW-2GPZwFZnTXwlX4kr8ihfMosVlZxdSaW-Q9ccsFul5p9W8ofAj-Ll0LvBoex7Re-EaTLP6A_57BYV_tYmCOX_SYI5EJHHhHGfw4-c8JjnlXXOmDPCsubl_PSs3imOQK29phXUvmc_y14e6ZP_2TSKTpNDunvst"

def check_df_data(df: pd.DataFrame) -> bool:
    # Check if dataframe is empty
    if df.empty:
        print("No songs downloaded. Finishing execution")
        return False

    # Primary Key Check
    if pd.Series(df['played_at']).is_unique:
        pass
    else:
        raise Exception("Primary Key violated")

    # Nulls check
    if df.isnull().values.any():
        raise Exception("Null values found")

    # Check that all timestamps are of yesterday's date
    yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
    yesterday = yesterday.replace(hour=0, minute=0, second=0, microsecond=0)

    timestamps = df["timestamp"].tolist()
    for timestamp in timestamps:
        if datetime.datetime.strptime(timestamp, '%Y-%m-%d') != yesterday:
            raise Exception("At least one of the returned songs does not have a yesterday's timestamp")

    return True

if __name__ =="__main__":
    # ETL part 1: Extract

    headers = {
        "Accept" : "application/json",
        "Content-Type" : "application/json",
        "Authorization" : "Bearer {token}".format(token=TOKEN)
    }

    # Convert time to Unix timestamp in miliseconds
    today = datetime.datetime.now()
    yesterday = today - datetime.timedelta(days=1)
    yesterday_unix_timestamp = int(yesterday.timestamp()) * 1000

    # Download all songs you've listened "after yesterday"
    r = requests.get("https://api.spotify.com/v1/me/player/recently-played?after={time}".format(time=yesterday_unix_timestamp), headers = headers)
    # r = requests.get("https://api.spotify.com/v1/me/player/recently-played", headers = headers)

    data = r.json()
    # print(data)
    #     for i in r:
    #         print(i)

    song_names = []
    artist_names = []
    played_at_list = []
    timestamps = []
    album = []
    album_release_date = []

    # Extract relevant data from the json object
    for song in data["items"]:
        song_names.append(song["track"]["name"])
        artist_names.append(song["track"]["album"]["artists"][0]["name"])
        album.append(song["track"]["album"]["name"])
        album_release_date.append(song["track"]["album"]["release_date"])
        played_at_list.append(song["played_at"])
        timestamps.append(song["played_at"][0:10])

    #     for key in song["track"]:
    #         print(key, ' <<<<->>>> ', song["track"][key])
    #     print('\n------------------------------------------------\n')

    # Dictionary to convert into a pandas dataframe
    song_dict = {
        "song_name" : song_names,
        "artist_name": artist_names,
        "album": album,
        "album_release": album_release_date,
        "played_at" : played_at_list,
        "timestamp" : timestamps
    }
    # for key in song_dict:
    #     print(key, ' - ', song_dict[key])

    df = pd.DataFrame(song_dict)
    print(df.head(50))

    # ETL part 2: Transform (Validate)

    if check_df_data(df):
        print("Datas are valid, proceed to Load stage")

    # ETL part 3: Load

    engine = sqlalchemy.create_engine(DATABASE_LOCATION)
    conn = sqlite3.connect('played_tracks.sqlite')
    cursor = conn.cursor()

    sql_query = """
    CREATE TABLE IF NOT EXISTS played_tracks(
        song_name VARCHAR(200),
        artist_name VARCHAR(200),
        album VARCHAR(200),
        album_release Varchar(200),
        played_at VARCHAR(200),
        timestamp VARCHAR(200),
        CONSTRAINT primary_key_constraint PRIMARY KEY (played_at)
    )
    """

    cursor.execute(sql_query)
    print("Opened database successfully")

    try:
        df.to_sql("played_tracks", engine, index=False, if_exists='append')
    except:
        print("Data already exists in the database")

    conn.close()
    print("Close database successfully")
